<?php

echo "Creating VPS\n";
$createData = array("name" => "VPN", "region" => "nyc3", "size" => "512mb", "image" => "ubuntu-14-04-x64", "ssh_keys" => array('1672431'));
$data_string = json_encode($createData);

$ch = curl_init('https://api.digitalocean.com/v2/droplets');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer 0e2bf2fc677225b03a12eef71df714b075b346f1f0b24865c78806cffb7f72d8',
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);

$result = curl_exec($ch);
$result = json_decode($result);
$dropletID = $result->droplet->id ;
echo "Successfully created a new VPS Instance\n";
echo "ID : " . $dropletID. "\n";
