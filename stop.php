<?php

$h = fopen('vpn.txt', 'r');
$line = fgetcsv($h);
$dropletID = $line[0];
$ip = $line[1];
fclose($h);

$data = array('id' => $dropletID);
$data_string = json_encode($data);
$ch = curl_init('https://api.digitalocean.com/v2/droplets');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer 0e2bf2fc677225b03a12eef71df714b075b346f1f0b24865c78806cffb7f72d8',
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);

$result = curl_exec($ch);
exec('rm *.txt');
exec('rm *.ovpn');
echo "Successfully destroyed VPN Server\n";