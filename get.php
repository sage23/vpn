<?php
echo "Sleeping for VPS boot\n";
sleep(60);
echo "Getting Droplet info\n";
$ch = curl_init('https://api.digitalocean.com/v2/droplets/'. $dropletID);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer 0e2bf2fc677225b03a12eef71df714b075b346f1f0b24865c78806cffb7f72d8',
    'Content-Type: application/json'));

$result = curl_exec($ch);
$result = json_decode($result);
$ip = $result->droplet->networks->v4[0]->ip_address;
echo "IP : " . $ip . "\n";
$h = fopen('vpn.txt', 'w');
fwrite($h, $dropletID . "," . $ip);
fclose($h);

echo "Successfully Started VPN Server\n";